FROM ubuntu:16.04

MAINTAINER omrigann@gmail.com

# Install required packages and remove the apt packages cache when done.

RUN apt-get update && \
    apt-get upgrade -y && \ 	
    apt-get install -y \
	git \
	curl \
	screen \
	build-essential \
	checkinstall \
	libreadline-gplv2-dev \
	libncursesw5-dev \
	libssl-dev \
	libsqlite3-dev \
	libgdbm-dev \
	libc6-dev \
	libbz2-dev \
	zlib1g-dev \
	openssl \
	tk-dev \
	libffi-dev \
	python3 \
	python3-dev \
	python3-setuptools \
	python3-pip \
	nginx \
	supervisor \
	sqlite3 && \
	pip3 install -U pip setuptools && \
   rm -rf /var/lib/apt/lists/*

# Install Python37
RUN mkdir /tmp/Python37

WORKDIR /tmp/Python37
RUN curl -O  https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz
RUN tar xvf Python-3.7.0.tar.xz

WORKDIR /tmp/Python37/Python-3.7.0
RUN ./configure
RUN make altinstall

# Install npm
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs

RUN pip3 install uwsgi

WORKDIR /